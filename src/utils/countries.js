export const countries = [
  {
    "id": 1,
    "name": "Afghanistan",
    "dial_code": 93,
    "country_code": "af",
    "name_ru": "\u0410\u0444\u0433\u0430\u043d\u0438\u0441\u0442\u0430\u043d",
    "name_en": "Afghanistan",
    "name_es": "Afganist\u00e1n",
    "name_de": "Afghanistan",
    "name_pl": "Afganistan"
  }, {
    "id": 2,
    "name": "\u00c5land Islands",
    "dial_code": 358,
    "country_code": "ax",
    "name_ru": "\u0410\u043b\u0430\u043d\u0434\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "\u00c5land Islands",
    "name_es": "Islas Aland",
    "name_de": "\u00c5land-Inseln",
    "name_pl": "Wyspy Alandzkie"
  }, {
    "id": 3,
    "name": "Albania",
    "dial_code": 355,
    "country_code": "al",
    "name_ru": "\u0410\u043b\u0431\u0430\u043d\u0438\u044f",
    "name_en": "Albania",
    "name_es": "Albania",
    "name_de": "Albanien",
    "name_pl": "Albania"
  }, {
    "id": 4,
    "name": "Algeria",
    "dial_code": 213,
    "country_code": "dz",
    "name_ru": "\u0410\u043b\u0436\u0438\u0440",
    "name_en": "Algeria",
    "name_es": "Argelia",
    "name_de": "Algerien",
    "name_pl": "Algieria"
  }, {
    "id": 5,
    "name": "American Samoa",
    "dial_code": 1684,
    "country_code": "as",
    "name_ru": "\u0410\u043c\u0435\u0440\u0438\u043a\u0430\u043d\u0441\u043a\u0438\u0439 \u0421\u0430\u043c\u043e\u0430",
    "name_en": "American Samoa",
    "name_es": "Samoa Americana",
    "name_de": "Amerikanisch-Samoa",
    "name_pl": "Samoa Ameryka\u0144skie"
  }, {
    "id": 6,
    "name": "Andorra",
    "dial_code": 376,
    "country_code": "ad",
    "name_ru": "\u0410\u043d\u0434\u043e\u0440\u0440\u0430",
    "name_en": "Andorra",
    "name_es": "Andorra",
    "name_de": "Andorra",
    "name_pl": "Andora"
  }, {
    "id": 7,
    "name": "Angola",
    "dial_code": 244,
    "country_code": "ao",
    "name_ru": "\u0410\u043d\u0433\u043e\u043b\u0430",
    "name_en": "Angola",
    "name_es": "Angola",
    "name_de": "Angola",
    "name_pl": "Angola"
  }, {
    "id": 8,
    "name": "Anguilla",
    "dial_code": 1264,
    "country_code": "ai",
    "name_ru": "\u0410\u043d\u0433\u0438\u043b\u044c\u044f",
    "name_en": "Anguilla",
    "name_es": "Anguila",
    "name_de": "Anguilla",
    "name_pl": "Anguilla"
  }, {
    "id": 9,
    "name": "Antarctica",
    "dial_code": 672,
    "country_code": "aq",
    "name_ru": "\u0410\u043d\u0442\u0430\u0440\u043a\u0442\u0438\u0434\u0430",
    "name_en": "Antarctica",
    "name_es": "Ant\u00e1rtida",
    "name_de": "Antarktis",
    "name_pl": "Antarktyda"
  }, {
    "id": 10,
    "name": "Antigua & Barbuda",
    "dial_code": 1268,
    "country_code": "ag",
    "name_ru": "\u0410\u043d\u0442\u0438\u0433\u0443\u0430 \u0438 \u0411\u0430\u0440\u0431\u0443\u0434\u0430",
    "name_en": "Antigua & Barbuda",
    "name_es": "Antigua y Barbuda",
    "name_de": "Antigua und Barbuda",
    "name_pl": "Antigua i Barbuda"
  }, {
    "id": 11,
    "name": "Argentina",
    "dial_code": 54,
    "country_code": "ar",
    "name_ru": "\u0410\u0440\u0433\u0435\u043d\u0442\u0438\u043d\u0430",
    "name_en": "Argentina",
    "name_es": "Argentina",
    "name_de": "Argentinien",
    "name_pl": "Argentyna"
  }, {
    "id": 12,
    "name": "Armenia",
    "dial_code": 374,
    "country_code": "am",
    "name_ru": "\u0410\u0440\u043c\u0435\u043d\u0438\u044f",
    "name_en": "Armenia",
    "name_es": "Armenia",
    "name_de": "Armenien",
    "name_pl": "Armenia"
  }, {
    "id": 13,
    "name": "Aruba",
    "dial_code": 297,
    "country_code": "aw",
    "name_ru": "\u0410\u0440\u0443\u0431\u0430",
    "name_en": "Aruba",
    "name_es": "Aruba",
    "name_de": "Aruba",
    "name_pl": "Aruba"
  }, {
    "id": 14,
    "name": "Australia",
    "dial_code": 61,
    "country_code": "au",
    "name_ru": "\u0410\u0432\u0441\u0442\u0440\u0430\u043b\u0438\u044f",
    "name_en": "Australia",
    "name_es": "Australia",
    "name_de": "Australien",
    "name_pl": "Australia"
  }, {
    "id": 15,
    "name": "Austria",
    "dial_code": 43,
    "country_code": "at",
    "name_ru": "\u0410\u0432\u0441\u0442\u0440\u0438\u044f",
    "name_en": "Austria",
    "name_es": "Austria",
    "name_de": "\u00d6sterreich",
    "name_pl": "Austria"
  }, {
    "id": 16,
    "name": "Azerbaijan",
    "dial_code": 994,
    "country_code": "az",
    "name_ru": "\u0410\u0437\u0435\u0440\u0431\u0430\u0439\u0434\u0436\u0430\u043d",
    "name_en": "Azerbaijan",
    "name_es": "Azerbaiy\u00e1n",
    "name_de": "Aserbaidschan",
    "name_pl": "Azerbejd\u017can"
  }, {
    "id": 17,
    "name": "Bahamas",
    "dial_code": 1242,
    "country_code": "bs",
    "name_ru": "\u0411\u0430\u0433\u0430\u043c\u044b",
    "name_en": "Bahamas",
    "name_es": "Bahamas",
    "name_de": "Bahamas",
    "name_pl": "Bahamy"
  }, {
    "id": 18,
    "name": "Bahrain",
    "dial_code": 973,
    "country_code": "bh",
    "name_ru": "\u0411\u0430\u0445\u0440\u0435\u0439\u043d",
    "name_en": "Bahrain",
    "name_es": "Bahrein",
    "name_de": "Bahrain",
    "name_pl": "Bahrajn"
  }, {
    "id": 19,
    "name": "Bangladesh",
    "dial_code": 880,
    "country_code": "bd",
    "name_ru": "\u0411\u0430\u043d\u0433\u043b\u0430\u0434\u0435\u0448",
    "name_en": "Bangladesh",
    "name_es": "Bangladesh",
    "name_de": "Bangladesch",
    "name_pl": "Bangladesz"
  }, {
    "id": 20,
    "name": "Barbados",
    "dial_code": 1246,
    "country_code": "bb",
    "name_ru": "\u0411\u0430\u0440\u0431\u0430\u0434\u043e\u0441",
    "name_en": "Barbados",
    "name_es": "Barbados",
    "name_de": "Barbados",
    "name_pl": "Barbados"
  }, {
    "id": 21,
    "name": "Belarus",
    "dial_code": 375,
    "country_code": "by",
    "name_ru": "\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u044c",
    "name_en": "Belarus",
    "name_es": "Bielorrusia",
    "name_de": "Wei\u00dfrussland",
    "name_pl": "Bia\u0142oru\u015b"
  }, {
    "id": 22,
    "name": "Belgium",
    "dial_code": 32,
    "country_code": "be",
    "name_ru": "\u0411\u0435\u043b\u044c\u0433\u0438\u044f",
    "name_en": "Belgium",
    "name_es": "B\u00e9lgica",
    "name_de": "Belgien",
    "name_pl": "Belgia"
  }, {
    "id": 23,
    "name": "Belize",
    "dial_code": 501,
    "country_code": "bz",
    "name_ru": "\u0411\u0435\u043b\u0438\u0437",
    "name_en": "Belize",
    "name_es": "Belice",
    "name_de": "Belize",
    "name_pl": "Belize"
  }, {
    "id": 24,
    "name": "Benin",
    "dial_code": 229,
    "country_code": "bj",
    "name_ru": "\u0411\u0435\u043d\u0438\u043d",
    "name_en": "Benin",
    "name_es": "Ben\u00edn",
    "name_de": "Benin",
    "name_pl": "Benin"
  }, {
    "id": 25,
    "name": "Bermuda",
    "dial_code": 1441,
    "country_code": "bm",
    "name_ru": "\u0411\u0435\u0440\u043c\u0443\u0434\u044b",
    "name_en": "Bermuda",
    "name_es": "Bermudas",
    "name_de": "Bermuda",
    "name_pl": "Bermudy"
  }, {
    "id": 26,
    "name": "Bhutan",
    "dial_code": 975,
    "country_code": "bt",
    "name_ru": "\u0411\u0443\u0442\u0430\u043d",
    "name_en": "Bhutan",
    "name_es": "But\u00e1n",
    "name_de": "Bhutan",
    "name_pl": "Bhutan"
  }, {
    "id": 27,
    "name": "Bolivia",
    "dial_code": 591,
    "country_code": "bo",
    "name_ru": "\u0411\u043e\u043b\u0438\u0432\u0438\u044f",
    "name_en": "Bolivia",
    "name_es": "Bolivia",
    "name_de": "Bolivien",
    "name_pl": "Boliwia"
  }, {
    "id": 28,
    "name": "Bosnia & Herzegovina",
    "dial_code": 387,
    "country_code": "ba",
    "name_ru": "\u0411\u043e\u0441\u043d\u0438\u044f \u0438 \u0413\u0435\u0440\u0446\u0435\u0433\u043e\u0432\u0438\u043d\u0430",
    "name_en": "Bosnia & Herzegovina",
    "name_es": "Bosnia y Herzegovina",
    "name_de": "Bosnien & Herzegowina",
    "name_pl": "Bo\u015bnia i Hercegowina"
  }, {
    "id": 29,
    "name": "Botswana",
    "dial_code": 267,
    "country_code": "bw",
    "name_ru": "\u0411\u043e\u0442\u0441\u0432\u0430\u043d\u0430",
    "name_en": "Botswana",
    "name_es": "Botswana",
    "name_de": "Botswana",
    "name_pl": "Botswana"
  }, {
    "id": 30,
    "name": "Bouvet Island",
    "dial_code": 55,
    "country_code": "bv",
    "name_ru": "\u041e\u0441\u0442\u0440\u043e\u0432 \u0411\u0443\u0432\u0435",
    "name_en": "Bouvet Island",
    "name_es": "Isla de Bouvet",
    "name_de": "Bouvetinsel",
    "name_pl": "Wyspa Bouveta"
  }, {
    "id": 31,
    "name": "Brazil",
    "dial_code": 55,
    "country_code": "br",
    "name_ru": "\u0411\u0440\u0430\u0437\u0438\u043b\u0438\u044f",
    "name_en": "Brazil",
    "name_es": "Brasil",
    "name_de": "Brasilien",
    "name_pl": "Brazylia"
  }, {
    "id": 32,
    "name": "Brunei",
    "dial_code": 673,
    "country_code": "bn",
    "name_ru": "\u0411\u0440\u0443\u043d\u0435\u0439",
    "name_en": "Brunei",
    "name_es": "Brunei",
    "name_de": "Brunei",
    "name_pl": "Brunei"
  }, {
    "id": 33,
    "name": "Bulgaria",
    "dial_code": 359,
    "country_code": "bg",
    "name_ru": "\u0411\u043e\u043b\u0433\u0430\u0440\u0438\u044f",
    "name_en": "Bulgaria",
    "name_es": "Bulgaria",
    "name_de": "Bulgarien",
    "name_pl": "Bu\u0142garia"
  }, {
    "id": 34,
    "name": "Burkina Faso",
    "dial_code": 226,
    "country_code": "bf",
    "name_ru": "\u0411\u0443\u0440\u043a\u0438\u043d\u0430-\u0424\u0430\u0441\u043e",
    "name_en": "Burkina Faso",
    "name_es": "Burkina Faso",
    "name_de": "Burkina Faso",
    "name_pl": "Burkina Faso"
  }, {
    "id": 35,
    "name": "Burundi",
    "dial_code": 257,
    "country_code": "bi",
    "name_ru": "\u0411\u0443\u0440\u0443\u043d\u0434\u0438",
    "name_en": "Burundi",
    "name_es": "Burundi",
    "name_de": "Burundi",
    "name_pl": "Burundi"
  }, {
    "id": 36,
    "name": "Cambodia",
    "dial_code": 855,
    "country_code": "kh",
    "name_ru": "\u041a\u0430\u043c\u0431\u043e\u0434\u0436\u0430",
    "name_en": "Cambodia",
    "name_es": "Camboya",
    "name_de": "Kambodscha",
    "name_pl": "Kambod\u017ca"
  }, {
    "id": 37,
    "name": "Cameroon",
    "dial_code": 237,
    "country_code": "cm",
    "name_ru": "\u041a\u0430\u043c\u0435\u0440\u0443\u043d",
    "name_en": "Cameroon",
    "name_es": "Camer\u00fan",
    "name_de": "Kamerun",
    "name_pl": "Kamerun"
  }, {
    "id": 38,
    "name": "Canada",
    "dial_code": 1,
    "country_code": "ca",
    "name_ru": "\u041a\u0430\u043d\u0430\u0434\u0430",
    "name_en": "Canada",
    "name_es": "Canad\u00e1",
    "name_de": "Kanada",
    "name_pl": "Kanada"
  }, {
    "id": 39,
    "name": "Cape Verde",
    "dial_code": 238,
    "country_code": "cv",
    "name_ru": "\u041a\u0430\u0431\u043e-\u0412\u0435\u0440\u0434\u0435",
    "name_en": "Cape Verde",
    "name_es": "Cabo Verde",
    "name_de": "Kap Verde",
    "name_pl": "Wyspy Zielonego Przyl\u0105dka"
  }, {
    "id": 40,
    "name": "Cayman Islands",
    "dial_code": 1345,
    "country_code": "ky",
    "name_ru": "\u041a\u0430\u0439\u043c\u0430\u043d\u043e\u0432\u044b \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "Cayman Islands",
    "name_es": "Islas Caim\u00e1n",
    "name_de": "Cayman-Inseln",
    "name_pl": "Kajmany"
  }, {
    "id": 41,
    "name": "Chad",
    "dial_code": 235,
    "country_code": "td",
    "name_ru": "\u0427\u0430\u0434",
    "name_en": "Chad",
    "name_es": "Chad",
    "name_de": "Tschad",
    "name_pl": "Czad"
  }, {
    "id": 42,
    "name": "Chile",
    "dial_code": 56,
    "country_code": "cl",
    "name_ru": "\u0427\u0438\u043b\u0438",
    "name_en": "Chile",
    "name_es": "Chile",
    "name_de": "Chile",
    "name_pl": "Chile"
  }, {
    "id": 43,
    "name": "China",
    "dial_code": 86,
    "country_code": "cn",
    "name_ru": "\u041a\u0438\u0442\u0430\u0439",
    "name_en": "China",
    "name_es": "China",
    "name_de": "China",
    "name_pl": "Chiny"
  }, {
    "id": 44,
    "name": "Christmas Island",
    "dial_code": 61,
    "country_code": "cx",
    "name_ru": "\u041e\u0441\u0442\u0440\u043e\u0432 \u0420\u043e\u0436\u0434\u0435\u0441\u0442\u0432\u0430",
    "name_en": "Christmas Island",
    "name_es": "Isla de Navidad",
    "name_de": "Weihnachtsinsel",
    "name_pl": "Wyspa Bo\u017cego Narodzenia"
  }, {
    "id": 45,
    "name": "Colombia",
    "dial_code": 57,
    "country_code": "co",
    "name_ru": "\u041a\u043e\u043b\u0443\u043c\u0431\u0438\u044f",
    "name_en": "Colombia",
    "name_es": "Colombia",
    "name_de": "Kolumbien",
    "name_pl": "Kolumbia"
  }, {
    "id": 46,
    "name": "Comoros",
    "dial_code": 269,
    "country_code": "km",
    "name_ru": "\u041a\u043e\u043c\u043e\u0440\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "Comoros",
    "name_es": "Comoras",
    "name_de": "Komoren",
    "name_pl": "Komory"
  }, {
    "id": 47,
    "name": "Congo",
    "dial_code": 180,
    "country_code": "cd",
    "name_ru": "\u041a\u043e\u043d\u0433\u043e",
    "name_en": "Congo",
    "name_es": "Congo",
    "name_de": "Kongo",
    "name_pl": "Kongo"
  }, {
    "id": 48,
    "name": "Cook Islands",
    "dial_code": 682,
    "country_code": "ck",
    "name_ru": "\u041e\u0441\u0442\u0440\u043e\u0432\u0430 \u041a\u0443\u043a\u0430",
    "name_en": "Cook Islands",
    "name_es": "Islas Cook",
    "name_de": "Cookinseln",
    "name_pl": "Wyspy Cooka"
  }, {
    "id": 49,
    "name": "Costa Rica",
    "dial_code": 506,
    "country_code": "cr",
    "name_ru": "\u041a\u043e\u0441\u0442\u0430-\u0420\u0438\u043a\u0430",
    "name_en": "Costa Rica",
    "name_es": "Costa Rica",
    "name_de": "Costa Rica",
    "name_pl": "Kostaryka"
  }, {
    "id": 50,
    "name": "Cote D Ivoire",
    "dial_code": 225,
    "country_code": "ci",
    "name_ru": "\u041a\u043e\u0442-\u0434'\u0418\u0432\u0443\u0430\u0440",
    "name_en": "Cote D Ivoire",
    "name_es": "Costa de Marfil",
    "name_de": "Elfenbeink\u00fcste",
    "name_pl": "Wybrze\u017ce Ko\u015bci S\u0142oniowej"
  }, {
    "id": 51,
    "name": "Croatia",
    "dial_code": 385,
    "country_code": "hr",
    "name_ru": "\u0425\u043e\u0440\u0432\u0430\u0442\u0438\u044f",
    "name_en": "Croatia",
    "name_es": "Croacia",
    "name_de": "Kroatien",
    "name_pl": "Chorwacja"
  }, {
    "id": 52,
    "name": "Cura\u00e7ao",
    "dial_code": 599,
    "country_code": "cw",
    "name_ru": "\u041a\u044e\u0440\u0430\u0441\u0430\u043e",
    "name_en": "Curacao",
    "name_es": "Cura\u00e7ao",
    "name_de": "Cura\u00e7ao",
    "name_pl": "Curacao"
  }, {
    "id": 53,
    "name": "Cyprus",
    "dial_code": 357,
    "country_code": "cy",
    "name_ru": "\u041a\u0438\u043f\u0440",
    "name_en": "Cyprus",
    "name_es": "Chipre",
    "name_de": "Zypern",
    "name_pl": "Cypr"
  }, {
    "id": 54,
    "name": "Czech Republic",
    "dial_code": 420,
    "country_code": "cz",
    "name_ru": "\u0427\u0435\u0445\u0438\u044f",
    "name_en": "Czech Republic",
    "name_es": "Rep\u00fablica Checa",
    "name_de": "Tschechische Republik",
    "name_pl": "Czechy"
  }, {
    "id": 55,
    "name": "Denmark",
    "dial_code": 45,
    "country_code": "dk",
    "name_ru": "\u0414\u0430\u043d\u0438\u044f",
    "name_en": "Denmark",
    "name_es": "Dinamarca",
    "name_de": "D\u00e4nemark",
    "name_pl": "Dania"
  }, {
    "id": 56,
    "name": "Djibouti",
    "dial_code": 253,
    "country_code": "dj",
    "name_ru": "\u0414\u0436\u0438\u0431\u0443\u0442\u0438",
    "name_en": "Djibouti",
    "name_es": "Yibuti",
    "name_de": "Dschibuti",
    "name_pl": "D\u017cibuti"
  }, {
    "id": 57,
    "name": "Dominica",
    "dial_code": 1767,
    "country_code": "dm",
    "name_ru": "\u0414\u043e\u043c\u0438\u043d\u0438\u043a\u0430",
    "name_en": "Dominica",
    "name_es": "Dominica",
    "name_de": "Dominica",
    "name_pl": "Dominika"
  }, {
    "id": 58,
    "name": "Dominican Republic",
    "dial_code": 1,
    "country_code": "do",
    "name_ru": "\u0414\u043e\u043c\u0438\u043d\u0438\u043a\u0430\u043d\u0441\u043a\u0430\u044f \u0420\u0435\u0441\u043f\u0431\u043b\u0438\u043a\u0430",
    "name_en": "Dominican Republic",
    "name_es": "Rep\u00fablica Dominicana",
    "name_de": "Dominikanische Republik",
    "name_pl": "Republika Dominika\u0144ska"
  }, {
    "id": 59,
    "name": "Ecuador",
    "dial_code": 593,
    "country_code": "ec",
    "name_ru": "\u042d\u043a\u0432\u0430\u0434\u043e\u0440",
    "name_en": "Ecuador",
    "name_es": "Ecuador",
    "name_de": "Ecuador",
    "name_pl": "Ekwador"
  }, {
    "id": 60,
    "name": "Egypt",
    "dial_code": 20,
    "country_code": "eg",
    "name_ru": "\u0415\u0433\u0438\u043f\u0435\u0442",
    "name_en": "Egypt",
    "name_es": "Egipto",
    "name_de": "\u00c4gypten",
    "name_pl": "Egipt"
  }, {
    "id": 61,
    "name": "El Salvador",
    "dial_code": 503,
    "country_code": "sv",
    "name_ru": "\u042d\u043b\u044c \u0421\u0430\u043b\u044c\u0432\u0430\u0434\u043e\u0440",
    "name_en": "El Salvador",
    "name_es": "El Salvador",
    "name_de": "El Salvador",
    "name_pl": "Salwador"
  }, {
    "id": 62,
    "name": "Equatorial Guinea",
    "dial_code": 240,
    "country_code": "gq",
    "name_ru": "\u042d\u043a\u0432\u0430\u0442\u043e\u0440\u0438\u0430\u043b\u044c\u043d\u0430\u044f \u0413\u0432\u0438\u043d\u0435\u044f",
    "name_en": "Equatorial Guinea",
    "name_es": "Guinea Ecuatorial",
    "name_de": "\u00c4quatorial-Guinea",
    "name_pl": "Gwinea R\u00f3wnikowa"
  }, {
    "id": 63,
    "name": "Eritrea",
    "dial_code": 291,
    "country_code": "er",
    "name_ru": "\u042d\u0440\u0438\u0442\u0440\u0435\u044f",
    "name_en": "Eritrea",
    "name_es": "Eritrea",
    "name_de": "Eritrea",
    "name_pl": "Erytrea"
  }, {
    "id": 64,
    "name": "Estonia",
    "dial_code": 372,
    "country_code": "ee",
    "name_ru": "\u042d\u0441\u0442\u043e\u043d\u0438\u044f",
    "name_en": "Estonia",
    "name_es": "Estonia",
    "name_de": "Estland",
    "name_pl": "Estonia"
  }, {
    "id": 65,
    "name": "Ethiopia",
    "dial_code": 251,
    "country_code": "et",
    "name_ru": "\u042d\u0444\u0438\u043e\u043f\u0438\u044f",
    "name_en": "Ethiopia",
    "name_es": "Etiop\u00eda",
    "name_de": "\u00c4thiopien",
    "name_pl": "Etiopia"
  }, {
    "id": 66,
    "name": "Faroe Islands",
    "dial_code": 298,
    "country_code": "fo",
    "name_ru": "\u0424\u0430\u0440\u0435\u0440\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "Faroe Islands",
    "name_es": "Islas Feroe",
    "name_de": "F\u00e4r\u00f6er Inseln",
    "name_pl": "Wyspy Owcze"
  }, {
    "id": 67,
    "name": "Fiji",
    "dial_code": 679,
    "country_code": "fj",
    "name_ru": "\u0424\u0438\u0434\u0436\u0438",
    "name_en": "Fiji",
    "name_es": "Fiji",
    "name_de": "Fidschi",
    "name_pl": "Fid\u017ci"
  }, {
    "id": 68,
    "name": "Finland",
    "dial_code": 358,
    "country_code": "fi",
    "name_ru": "\u0424\u0438\u043d\u043b\u044f\u043d\u0434\u0438\u044f",
    "name_en": "Finland",
    "name_es": "Finlandia",
    "name_de": "Finnland",
    "name_pl": "Finlandia"
  }, {
    "id": 69,
    "name": "France",
    "dial_code": 33,
    "country_code": "fr",
    "name_ru": "\u0424\u0440\u0430\u043d\u0446\u0438\u044f",
    "name_en": "France",
    "name_es": "Francia",
    "name_de": "Frankreich",
    "name_pl": "Francja"
  }, {
    "id": 70,
    "name": "French Guiana",
    "dial_code": 594,
    "country_code": "gf",
    "name_ru": "\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0430\u044f \u0413\u0432\u0438\u0430\u043d\u0430",
    "name_en": "French Guiana",
    "name_es": "Guayana Francesa",
    "name_de": "Franz\u00f6sisch-Guayana",
    "name_pl": "Gujana Francuska"
  }, {
    "id": 71,
    "name": "French Polynesia",
    "dial_code": 689,
    "country_code": "pf",
    "name_ru": "\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0430\u044f \u041f\u043e\u043b\u0438\u043d\u0435\u0437\u0438\u044f",
    "name_en": "French Polynesia",
    "name_es": "Polinesia Francesa",
    "name_de": "Franz\u00f6sisch-Polynesien",
    "name_pl": "Polinezja Francuska"
  }, {
    "id": 72,
    "name": "Gabon",
    "dial_code": 241,
    "country_code": "ga",
    "name_ru": "\u0413\u0430\u0431\u043e\u043d",
    "name_en": "Gabon",
    "name_es": "Gab\u00f3n",
    "name_de": "Gabun",
    "name_pl": "Gabon"
  }, {
    "id": 73,
    "name": "Gambia",
    "dial_code": 220,
    "country_code": "gm",
    "name_ru": "\u0413\u0430\u043c\u0431\u0438\u044f",
    "name_en": "Gambia",
    "name_es": "Gambia",
    "name_de": "Gambia",
    "name_pl": "Gambia"
  }, {
    "id": 74,
    "name": "Georgia",
    "dial_code": 995,
    "country_code": "ge",
    "name_ru": "\u0413\u0440\u0443\u0437\u0438\u044f",
    "name_en": "Georgia",
    "name_es": "Georgia",
    "name_de": "Georgien",
    "name_pl": "Gruzja"
  }, {
    "id": 75,
    "name": "Germany",
    "dial_code": 49,
    "country_code": "de",
    "name_ru": "\u0413\u0435\u0440\u043c\u0430\u043d\u0438\u044f",
    "name_en": "Germany",
    "name_es": "Alemania",
    "name_de": "Deutschland",
    "name_pl": "Niemcy"
  }, {
    "id": 76,
    "name": "Ghana",
    "dial_code": 233,
    "country_code": "gh",
    "name_ru": "\u0413\u0430\u043d\u0430",
    "name_en": "Ghana",
    "name_es": "Ghana",
    "name_de": "Ghana",
    "name_pl": "Ghana"
  }, {
    "id": 77,
    "name": "Gibraltar",
    "dial_code": 350,
    "country_code": "gi",
    "name_ru": "\u0413\u0438\u0431\u0440\u0430\u043b\u0442\u0430\u0440",
    "name_en": "Gibraltar",
    "name_es": "Gibraltar",
    "name_de": "Gibraltar",
    "name_pl": "Gibraltar"
  }, {
    "id": 78,
    "name": "Greece",
    "dial_code": 30,
    "country_code": "gr",
    "name_ru": "\u0413\u0440\u0435\u0446\u0438\u044f",
    "name_en": "Greece",
    "name_es": "Grecia",
    "name_de": "Griechenland",
    "name_pl": "Grecja"
  }, {
    "id": 79,
    "name": "Greenland",
    "dial_code": 299,
    "country_code": "gl",
    "name_ru": "\u0413\u0440\u0435\u043d\u043b\u0430\u043d\u0434\u0438\u044f",
    "name_en": "Greenland",
    "name_es": "Groenlandia",
    "name_de": "Gr\u00f6nland",
    "name_pl": "Grenlandia"
  }, {
    "id": 80,
    "name": "Grenada",
    "dial_code": 1473,
    "country_code": "gd",
    "name_ru": "\u0413\u0440\u0435\u043d\u0430\u0434\u0430",
    "name_en": "Grenada",
    "name_es": "Granada",
    "name_de": "Grenada",
    "name_pl": "Grenada"
  }, {
    "id": 81,
    "name": "Guadeloupe",
    "dial_code": 590,
    "country_code": "gp",
    "name_ru": "\u0413\u0432\u0430\u0434\u0435\u043b\u0443\u043f\u0430",
    "name_en": "Guadeloupe",
    "name_es": "Guadalupe",
    "name_de": "Guadeloupe",
    "name_pl": "Gwadelupa"
  }, {
    "id": 82,
    "name": "Guam",
    "dial_code": 1671,
    "country_code": "gu",
    "name_ru": "\u0413\u0443\u0430\u043c",
    "name_en": "Guam",
    "name_es": "Guam",
    "name_de": "Guam",
    "name_pl": "Guam"
  }, {
    "id": 83,
    "name": "Guatemala",
    "dial_code": 502,
    "country_code": "gt",
    "name_ru": "\u0413\u0432\u0430\u0442\u0435\u043c\u0430\u043b\u0430",
    "name_en": "Guatemala",
    "name_es": "Guatemala",
    "name_de": "Guatemala",
    "name_pl": "Gwatemala"
  }, {
    "id": 84,
    "name": "Guernsey",
    "dial_code": 44,
    "country_code": "gg",
    "name_ru": "\u0413\u0435\u0440\u043d\u0441\u0438",
    "name_en": "Guernsey",
    "name_es": "Guernsey",
    "name_de": "Guernsey",
    "name_pl": "Guernsey"
  }, {
    "id": 85,
    "name": "Guinea",
    "dial_code": 224,
    "country_code": "gn",
    "name_ru": "\u0413\u0432\u0438\u043d\u0435\u044f",
    "name_en": "Guinea",
    "name_es": "Guinea",
    "name_de": "Guinea",
    "name_pl": "Gwinea"
  }, {
    "id": 86,
    "name": "Guinea-Bissau",
    "dial_code": 245,
    "country_code": "gw",
    "name_ru": "\u0413\u0432\u0438\u043d\u0435\u044f-\u0411\u0438\u0441\u0430\u0443",
    "name_en": "Guinea-Bissau",
    "name_es": "Guinea-Bissau",
    "name_de": "Guinea-Bissau",
    "name_pl": "Gwinea Bissau"
  }, {
    "id": 87,
    "name": "Guyana",
    "dial_code": 592,
    "country_code": "gy",
    "name_ru": "\u0413\u0430\u0439\u0430\u043d\u0430",
    "name_en": "Guyana",
    "name_es": "Guyana",
    "name_de": "Guyana",
    "name_pl": "Gujana"
  }, {
    "id": 88,
    "name": "Haiti",
    "dial_code": 509,
    "country_code": "ht",
    "name_ru": "\u0413\u0430\u0438\u0442\u0438",
    "name_en": "Haiti",
    "name_es": "Hait\u00ed",
    "name_de": "Haiti",
    "name_pl": "Haiti"
  }, {
    "id": 89,
    "name": "Heard Island",
    "dial_code": 0,
    "country_code": "hm",
    "name_ru": "\u041e\u0441\u0442\u0440\u043e\u0432 \u0425\u0435\u0440\u0434",
    "name_en": "Heard Island",
    "name_es": "Islas Marianas del Norte",
    "name_de": "Heard-Insel",
    "name_pl": "Wyspa Heard"
  }, {
    "id": 90,
    "name": "Holy See",
    "dial_code": 379,
    "country_code": "va",
    "name_ru": "\u0421\u0432\u044f\u0442\u043e\u0439 \u041f\u0440\u0435\u0441\u0442\u043e\u043b",
    "name_en": "Holy See",
    "name_es": "Santa Sede",
    "name_de": "Heiliger Stuhl",
    "name_pl": "Stolica Apostolska"
  }, {
    "id": 91,
    "name": "Honduras",
    "dial_code": 504,
    "country_code": "hn",
    "name_ru": "\u0413\u043e\u043d\u0434\u0443\u0440\u0430\u0441",
    "name_en": "Honduras",
    "name_es": "Honduras",
    "name_de": "Honduras",
    "name_pl": "Honduras"
  }, {
    "id": 92,
    "name": "Hong Kong",
    "dial_code": 852,
    "country_code": "hk",
    "name_ru": "\u0413\u043e\u043d\u043a\u043e\u043d\u0433",
    "name_en": "Hong Kong",
    "name_es": "Hong Kong",
    "name_de": "Hongkong",
    "name_pl": "Hongkong"
  }, {
    "id": 93,
    "name": "Hungary",
    "dial_code": 36,
    "country_code": "hu",
    "name_ru": "\u0412\u0435\u043d\u0433\u0440\u0438\u044f",
    "name_en": "Hungary",
    "name_es": "Hungr\u00eda",
    "name_de": "Ungarn",
    "name_pl": "W\u0119gry"
  }, {
    "id": 94,
    "name": "Iceland",
    "dial_code": 354,
    "country_code": "is",
    "name_ru": "\u0418\u0441\u043b\u0430\u043d\u0434\u0438\u044f",
    "name_en": "Iceland",
    "name_es": "Islandia",
    "name_de": "Island",
    "name_pl": "Islandia"
  }, {
    "id": 95,
    "name": "India",
    "dial_code": 91,
    "country_code": "in",
    "name_ru": "\u0418\u043d\u0434\u0438\u044f",
    "name_en": "India",
    "name_es": "India",
    "name_de": "Indien",
    "name_pl": "Indie"
  }, {
    "id": 96,
    "name": "Indonesia",
    "dial_code": 62,
    "country_code": "id",
    "name_ru": "\u0418\u043d\u0434\u043e\u043d\u0435\u0437\u0438\u044f",
    "name_en": "Indonesia",
    "name_es": "Indonesia",
    "name_de": "Indonesien",
    "name_pl": "Indonezja"
  }, {
    "id": 97,
    "name": "Iraq",
    "dial_code": 964,
    "country_code": "iq",
    "name_ru": "\u0418\u0440\u0430\u043a",
    "name_en": "Iraq",
    "name_es": "Iraq",
    "name_de": "Irak",
    "name_pl": "Irak"
  }, {
    "id": 98,
    "name": "Ireland",
    "dial_code": 353,
    "country_code": "ie",
    "name_ru": "\u0418\u0440\u043b\u0430\u043d\u0434\u0438\u044f",
    "name_en": "Ireland",
    "name_es": "Irlanda",
    "name_de": "Irland",
    "name_pl": "Irlandia"
  }, {
    "id": 99,
    "name": "Isle of Man",
    "dial_code": 44,
    "country_code": "im",
    "name_ru": "\u041e\u0441\u0442\u0440\u043e\u0432 \u041c\u044d\u043d",
    "name_en": "Isle of Man",
    "name_es": "Isla de Man",
    "name_de": "Isle of Man",
    "name_pl": "Wyspa Man"
  }, {
    "id": 100,
    "name": "Israel",
    "dial_code": 972,
    "country_code": "il",
    "name_ru": "\u0418\u0437\u0440\u0430\u0438\u043b\u044c",
    "name_en": "Israel",
    "name_es": "Israel",
    "name_de": "Israel",
    "name_pl": "Izrael"
  }, {
    "id": 101,
    "name": "Italy",
    "dial_code": 39,
    "country_code": "it",
    "name_ru": "\u0418\u0442\u0430\u043b\u0438\u044f",
    "name_en": "Italy",
    "name_es": "Italia",
    "name_de": "Italien",
    "name_pl": "W\u0142ochy"
  }, {
    "id": 102,
    "name": "Jamaica",
    "dial_code": 1876,
    "country_code": "jm",
    "name_ru": "\u042f\u043c\u0430\u0439\u043a\u0430",
    "name_en": "Jamaica",
    "name_es": "Jamaica",
    "name_de": "Jamaika",
    "name_pl": "Jamajka"
  }, {
    "id": 103,
    "name": "Japan",
    "dial_code": 81,
    "country_code": "jp",
    "name_ru": "\u042f\u043f\u043e\u043d\u0438\u044f",
    "name_en": "Japan",
    "name_es": "Jap\u00f3n",
    "name_de": "Japan",
    "name_pl": "Japonia"
  }, {
    "id": 104,
    "name": "Jersey",
    "dial_code": 44,
    "country_code": "je",
    "name_ru": "\u0414\u0436\u0435\u0440\u0441\u0438",
    "name_en": "Jersey",
    "name_es": "Jersey",
    "name_de": "Jersey",
    "name_pl": "Jersey"
  }, {
    "id": 105,
    "name": "Jordan",
    "dial_code": 962,
    "country_code": "jo",
    "name_ru": "\u0418\u043e\u0440\u0434\u0430\u043d\u0438\u044f",
    "name_en": "Jordan",
    "name_es": "Jordania",
    "name_de": "Jordanien",
    "name_pl": "Jordan"
  }, {
    "id": 106,
    "name": "Kazakhstan",
    "dial_code": 7,
    "country_code": "kz",
    "name_ru": "\u041a\u0430\u0437\u0430\u0445\u0441\u0442\u0430\u043d",
    "name_en": "Kazakhstan",
    "name_es": "Kazajist\u00e1n",
    "name_de": "Kasachstan",
    "name_pl": "Kazachstan"
  }, {
    "id": 107,
    "name": "Kenya",
    "dial_code": 254,
    "country_code": "ke",
    "name_ru": "\u041a\u0435\u043d\u0438\u044f",
    "name_en": "Kenya",
    "name_es": "Kenia",
    "name_de": "Kenia",
    "name_pl": "Kenia"
  }, {
    "id": 108,
    "name": "Kiribati",
    "dial_code": 686,
    "country_code": "ki",
    "name_ru": "\u041a\u0438\u0440\u0438\u0431\u0430\u0442\u0438",
    "name_en": "Kiribati",
    "name_es": "Kiribati",
    "name_de": "Kiribati",
    "name_pl": "Kiribati"
  }, {
    "id": 109,
    "name": "Korea (Republic of)",
    "dial_code": 82,
    "country_code": "kr",
    "name_ru": "\u0420\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430 \u041a\u043e\u0440\u0435\u044f",
    "name_en": "Korea (Republic of)",
    "name_es": "Rep\u00fablica de Corea",
    "name_de": "Korea (Republik)",
    "name_pl": "Korea Po\u0142udniowa"
  }, {
    "id": 110,
    "name": "Kuwait",
    "dial_code": 965,
    "country_code": "kw",
    "name_ru": "\u041a\u0443\u0432\u0435\u0439\u0442",
    "name_en": "Kuwait",
    "name_es": "Kuwait",
    "name_de": "Kuwait",
    "name_pl": "Kuwejt"
  }, {
    "id": 111,
    "name": "Kyrgyzstan",
    "dial_code": 996,
    "country_code": "kg",
    "name_ru": "\u041a\u044b\u0440\u0433\u044b\u0437\u0441\u0442\u0430\u043d",
    "name_en": "Kyrgyzstan",
    "name_es": "Kirguist\u00e1n",
    "name_de": "Kirgisistan",
    "name_pl": "Kirgistan"
  }, {
    "id": 112,
    "name": "Lao",
    "dial_code": 856,
    "country_code": "la",
    "name_ru": "\u041b\u0430\u043e\u0441",
    "name_en": "Laos",
    "name_es": "Laos",
    "name_de": "Laos",
    "name_pl": "Laos"
  }, {
    "id": 113,
    "name": "Latvia",
    "dial_code": 371,
    "country_code": "lv",
    "name_ru": "\u041b\u0430\u0442\u0432\u0438\u044f",
    "name_en": "Latvia",
    "name_es": "Letonia",
    "name_de": "Lettland",
    "name_pl": "\u0141otwa"
  }, {
    "id": 114,
    "name": "Lebanon",
    "dial_code": 961,
    "country_code": "lb",
    "name_ru": "\u041b\u0438\u0432\u0430\u043d",
    "name_en": "Lebanon",
    "name_es": "L\u00edbano",
    "name_de": "Libanon",
    "name_pl": "Liban"
  }, {
    "id": 115,
    "name": "Lesotho",
    "dial_code": 266,
    "country_code": "ls",
    "name_ru": "\u041b\u0435\u0441\u043e\u0442\u043e",
    "name_en": "Lesotho",
    "name_es": "Lesotho",
    "name_de": "Lesotho",
    "name_pl": "Lesotho"
  }, {
    "id": 116,
    "name": "Liberia",
    "dial_code": 231,
    "country_code": "lr",
    "name_ru": "\u041b\u0438\u0431\u0435\u0440\u0438\u044f",
    "name_en": "Liberia",
    "name_es": "Liberia",
    "name_de": "Liberia",
    "name_pl": "Liberia"
  }, {
    "id": 117,
    "name": "Libya",
    "dial_code": 218,
    "country_code": "ly",
    "name_ru": "\u041b\u0438\u0432\u0438\u044f",
    "name_en": "Libya",
    "name_es": "Libia",
    "name_de": "Libyen",
    "name_pl": "Libia"
  }, {
    "id": 118,
    "name": "Liechtenstein",
    "dial_code": 423,
    "country_code": "li",
    "name_ru": "\u041b\u0438\u0445\u0442\u0435\u043d\u0448\u0442\u0435\u0439\u043d",
    "name_en": "Liechtenstein",
    "name_es": "Liechtenstein",
    "name_de": "Liechtenstein",
    "name_pl": "Liechtenstein"
  }, {
    "id": 119,
    "name": "Lithuania",
    "dial_code": 370,
    "country_code": "lt",
    "name_ru": "\u041b\u0438\u0442\u0432\u0430",
    "name_en": "Lithuania",
    "name_es": "Lituania",
    "name_de": "Litauen",
    "name_pl": "Litwa"
  }, {
    "id": 120,
    "name": "Luxembourg",
    "dial_code": 352,
    "country_code": "lu",
    "name_ru": "\u041b\u044e\u043a\u0441\u0435\u043c\u0431\u0443\u0440\u0433",
    "name_en": "Luxembourg",
    "name_es": "Luxemburgo",
    "name_de": "Luxemburg",
    "name_pl": "Luksemburg"
  }, {
    "id": 121,
    "name": "Macao",
    "dial_code": 853,
    "country_code": "mo",
    "name_ru": "\u041c\u0430\u043a\u0430\u043e",
    "name_en": "Macao",
    "name_es": "Macao",
    "name_de": "Macau",
    "name_pl": "Makau"
  }, {
    "id": 122,
    "name": "Macedonia",
    "dial_code": 389,
    "country_code": "mk",
    "name_ru": "\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0438\u044f",
    "name_en": "Macedonia",
    "name_es": "Macedonia",
    "name_de": "Mazedonien",
    "name_pl": "Macedonia"
  }, {
    "id": 123,
    "name": "Madagascar",
    "dial_code": 261,
    "country_code": "mg",
    "name_ru": "\u041c\u0430\u0434\u0430\u0433\u0430\u0441\u043a\u0430\u0440",
    "name_en": "Madagascar",
    "name_es": "Madagascar",
    "name_de": "Madagaskar",
    "name_pl": "Madagaskar"
  }, {
    "id": 124,
    "name": "Malawi",
    "dial_code": 265,
    "country_code": "mw",
    "name_ru": "\u041c\u0430\u043b\u0430\u0432\u0438",
    "name_en": "Malawi",
    "name_es": "Malawi",
    "name_de": "Malawi",
    "name_pl": "Malawi"
  }, {
    "id": 125,
    "name": "Malaysia",
    "dial_code": 60,
    "country_code": "my",
    "name_ru": "\u041c\u0430\u043b\u0430\u0439\u0437\u0438\u044f",
    "name_en": "Malaysia",
    "name_es": "Malasia",
    "name_de": "Malaysia",
    "name_pl": "Malezja"
  }, {
    "id": 126,
    "name": "Maldives",
    "dial_code": 960,
    "country_code": "mv",
    "name_ru": "\u041c\u0430\u043b\u044c\u0434\u0438\u0432\u044b",
    "name_en": "Maldives",
    "name_es": "Maldivas",
    "name_de": "Malediven",
    "name_pl": "Malediwy"
  }, {
    "id": 127,
    "name": "Mali",
    "dial_code": 223,
    "country_code": "ml",
    "name_ru": "\u041c\u0430\u043b\u0438",
    "name_en": "Mali",
    "name_es": "Mali",
    "name_de": "Mali",
    "name_pl": "Mali"
  }, {
    "id": 128,
    "name": "Malta",
    "dial_code": 356,
    "country_code": "mt",
    "name_ru": "\u041c\u0430\u043b\u044c\u0442\u0430",
    "name_en": "Malta",
    "name_es": "Malta",
    "name_de": "Malta",
    "name_pl": "Malta"
  }, {
    "id": 129,
    "name": "Marshall Islands",
    "dial_code": 692,
    "country_code": "mh",
    "name_ru": "\u041c\u0430\u0440\u0448\u0430\u043b\u043b\u043e\u0432\u044b \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "Marshall Islands",
    "name_es": "Islas Marshall",
    "name_de": "Marshall-Inseln",
    "name_pl": "Wyspy Marshalla"
  }, {
    "id": 130,
    "name": "Martinique",
    "dial_code": 596,
    "country_code": "mq",
    "name_ru": "\u041c\u0430\u0440\u0442\u0438\u043d\u0438\u043a\u0430",
    "name_en": "Martinique",
    "name_es": "Martinica",
    "name_de": "Martinique",
    "name_pl": "Martynika"
  }, {
    "id": 131,
    "name": "Mauritania",
    "dial_code": 222,
    "country_code": "mr",
    "name_ru": "\u041c\u0430\u0432\u0440\u0438\u0442\u0430\u043d\u0438\u044f",
    "name_en": "Mauritania",
    "name_es": "Mauritania",
    "name_de": "Mauretanien",
    "name_pl": "Mauretania"
  }, {
    "id": 132,
    "name": "Mauritius",
    "dial_code": 230,
    "country_code": "mu",
    "name_ru": "\u041c\u0430\u0432\u0440\u0438\u043a\u0438\u0439",
    "name_en": "Mauritius",
    "name_es": "Mauricio",
    "name_de": "Mauritius",
    "name_pl": "Mauritius"
  }, {
    "id": 133,
    "name": "Mayotte",
    "dial_code": 262,
    "country_code": "yt",
    "name_ru": "\u041c\u0430\u0439\u043e\u0442\u0442\u0430",
    "name_en": "Mayotte",
    "name_es": "Mayotte",
    "name_de": "Mayotte",
    "name_pl": "Majotta"
  }, {
    "id": 134,
    "name": "Mexico",
    "dial_code": 52,
    "country_code": "mx",
    "name_ru": "\u041c\u0435\u043a\u0441\u0438\u043a\u0430",
    "name_en": "Mexico",
    "name_es": "M\u00e9xico",
    "name_de": "Mexiko",
    "name_pl": "Meksyk"
  }, {
    "id": 135,
    "name": "Micronesia",
    "dial_code": 691,
    "country_code": "fm",
    "name_ru": "\u041c\u0438\u043a\u0440\u043e\u043d\u0435\u0437\u0438\u044f",
    "name_en": "Micronesia",
    "name_es": "Micronesia",
    "name_de": "Mikronesien",
    "name_pl": "Mikronezja"
  }, {
    "id": 136,
    "name": "Moldova",
    "dial_code": 373,
    "country_code": "md",
    "name_ru": "\u041c\u043e\u043b\u0434\u043e\u0432\u0430",
    "name_en": "Moldova",
    "name_es": "Moldavia",
    "name_de": "Moldawien",
    "name_pl": "Mo\u0142dawia"
  }, {
    "id": 137,
    "name": "Monaco",
    "dial_code": 377,
    "country_code": "mc",
    "name_ru": "\u041c\u043e\u043d\u0430\u043a\u043e",
    "name_en": "Monaco",
    "name_es": "M\u00f3naco",
    "name_de": "Monaco",
    "name_pl": "Monako"
  }, {
    "id": 138,
    "name": "Mongolia",
    "dial_code": 976,
    "country_code": "mn",
    "name_ru": "\u041c\u043e\u043d\u0433\u043e\u043b\u0438\u044f",
    "name_en": "Mongolia",
    "name_es": "Mongolia",
    "name_de": "Mongolei",
    "name_pl": "Mongolia"
  }, {
    "id": 139,
    "name": "Montenegro",
    "dial_code": 382,
    "country_code": "me",
    "name_ru": "\u0427\u0435\u0440\u043d\u043e\u0433\u043e\u0440\u0438\u044f",
    "name_en": "Montenegro",
    "name_es": "Montenegro",
    "name_de": "Montenegro",
    "name_pl": "Czarnog\u00f3ra"
  }, {
    "id": 140,
    "name": "Montserrat",
    "dial_code": 1664,
    "country_code": "ms",
    "name_ru": "\u041c\u043e\u043d\u0441\u0435\u0440\u0440\u0430\u0442",
    "name_en": "Montserrat",
    "name_es": "Montserrat",
    "name_de": "Montserrat",
    "name_pl": "Montserrat"
  }, {
    "id": 141,
    "name": "Morocco",
    "dial_code": 212,
    "country_code": "ma",
    "name_ru": "\u041c\u0430\u0440\u043e\u043a\u043a\u043e",
    "name_en": "Morocco",
    "name_es": "Marruecos",
    "name_de": "Marokko",
    "name_pl": "Maroko"
  }, {
    "id": 142,
    "name": "Mozambique",
    "dial_code": 258,
    "country_code": "mz",
    "name_ru": "\u041c\u043e\u0437\u0430\u043c\u0431\u0438\u043a",
    "name_en": "Mozambique",
    "name_es": "Mozambique",
    "name_de": "Mosambik",
    "name_pl": "Mozambik"
  }, {
    "id": 143,
    "name": "Myanmar",
    "dial_code": 95,
    "country_code": "mm",
    "name_ru": "\u041c\u044c\u044f\u043d\u043c\u0430",
    "name_en": "Myanmar",
    "name_es": "Myanmar",
    "name_de": "Myanmar",
    "name_pl": "Myanmar"
  }, {
    "id": 144,
    "name": "Namibia",
    "dial_code": 264,
    "country_code": "na",
    "name_ru": "\u041d\u0430\u043c\u0438\u0431\u0438\u044f",
    "name_en": "Namibia",
    "name_es": "Namibia",
    "name_de": "Namibia",
    "name_pl": "Namibia"
  }, {
    "id": 145,
    "name": "Nauru",
    "dial_code": 674,
    "country_code": "nr",
    "name_ru": "\u041d\u0430\u0443\u0440\u0443",
    "name_en": "Nauru",
    "name_es": "Nauru",
    "name_de": "Nauru",
    "name_pl": "Nauru"
  }, {
    "id": 146,
    "name": "Nepal",
    "dial_code": 977,
    "country_code": "np",
    "name_ru": "\u041d\u0435\u043f\u0430\u043b",
    "name_en": "Nepal",
    "name_es": "Nepal",
    "name_de": "Nepal",
    "name_pl": "Nepal"
  }, {
    "id": 147,
    "name": "Netherlands",
    "dial_code": 31,
    "country_code": "nl",
    "name_ru": "\u041d\u0438\u0434\u0435\u0440\u043b\u0430\u043d\u0434\u044b",
    "name_en": "Netherlands",
    "name_es": "Pa\u00edses Bajos",
    "name_de": "Niederlande",
    "name_pl": "Holandia"
  }, {
    "id": 148,
    "name": "New Caledonia",
    "dial_code": 687,
    "country_code": "nc",
    "name_ru": "\u041d\u043e\u0432\u0430\u044f \u041a\u0430\u043b\u0435\u0434\u043e\u043d\u0438\u044f",
    "name_en": "New Caledonia",
    "name_es": "Nueva Caledonia",
    "name_de": "Neukaledonien",
    "name_pl": "Nowa Kaledonia"
  }, {
    "id": 149,
    "name": "New Zealand",
    "dial_code": 64,
    "country_code": "nz",
    "name_ru": "\u041d\u043e\u0432\u0430\u044f \u0417\u0435\u043b\u0430\u043d\u0434\u0438\u044f",
    "name_en": "New Zealand",
    "name_es": "Nueva Zelanda",
    "name_de": "Neu Seeland",
    "name_pl": "Nowa Zelandia"
  }, {
    "id": 150,
    "name": "Nicaragua",
    "dial_code": 505,
    "country_code": "ni",
    "name_ru": "\u041d\u0438\u043a\u0430\u0440\u0430\u0433\u0443\u0430",
    "name_en": "Nicaragua",
    "name_es": "Nicaragua",
    "name_de": "Nicaragua",
    "name_pl": "Nikaragua"
  }, {
    "id": 151,
    "name": "Niger",
    "dial_code": 227,
    "country_code": "ne",
    "name_ru": "\u041d\u0438\u0433\u0435\u0440",
    "name_en": "Niger",
    "name_es": "N\u00edger",
    "name_de": "Niger",
    "name_pl": "Niger"
  }, {
    "id": 152,
    "name": "Nigeria",
    "dial_code": 234,
    "country_code": "ng",
    "name_ru": "\u041d\u0438\u0433\u0435\u0440\u0438\u044f",
    "name_en": "Nigeria",
    "name_es": "Nigeria",
    "name_de": "Nigeria",
    "name_pl": "Nigeria"
  }, {
    "id": 153,
    "name": "Niue",
    "dial_code": 683,
    "country_code": "nu",
    "name_ru": "\u041d\u0438\u0443\u044d",
    "name_en": "Niue",
    "name_es": "Niue",
    "name_de": "Niue",
    "name_pl": "Niue"
  }, {
    "id": 154,
    "name": "Norfolk Island",
    "dial_code": 672,
    "country_code": "nf",
    "name_ru": "\u041e\u0441\u0442\u0440\u043e\u0432 \u041d\u043e\u0440\u0444\u043e\u043b\u043a",
    "name_en": "Norfolk Island",
    "name_es": "Isla de Norfolk",
    "name_de": "Norfolkinsel",
    "name_pl": "Wyspa Norfolk"
  }, {
    "id": 155,
    "name": "Norway",
    "dial_code": 47,
    "country_code": "no",
    "name_ru": "\u041d\u043e\u0440\u0432\u0435\u0433\u0438\u044f",
    "name_en": "Norway",
    "name_es": "Noruega",
    "name_de": "Norwegen",
    "name_pl": "Norwegia"
  }, {
    "id": 156,
    "name": "Oman",
    "dial_code": 968,
    "country_code": "om",
    "name_ru": "\u041e\u043c\u0430\u043d",
    "name_en": "Oman",
    "name_es": "Om\u00e1n",
    "name_de": "Oman",
    "name_pl": "Oman"
  }, {
    "id": 157,
    "name": "Pakistan",
    "dial_code": 92,
    "country_code": "pk",
    "name_ru": "\u041f\u0430\u043a\u0438\u0441\u0442\u0430\u043d",
    "name_en": "Pakistan",
    "name_es": "Pakist\u00e1n",
    "name_de": "Pakistan",
    "name_pl": "Pakistan"
  }, {
    "id": 158,
    "name": "Palau",
    "dial_code": 680,
    "country_code": "pw",
    "name_ru": "\u041f\u0430\u043b\u0430\u0443",
    "name_en": "Palau",
    "name_es": "Palau",
    "name_de": "Palau",
    "name_pl": "Palau"
  }, {
    "id": 159,
    "name": "Palestine",
    "dial_code": 970,
    "country_code": "ps",
    "name_ru": "\u041f\u0430\u043b\u0435\u0441\u0442\u0438\u043d\u0430",
    "name_en": "Palestine",
    "name_es": "Palestina",
    "name_de": "Pal\u00e4stina",
    "name_pl": "Palestyna"
  }, {
    "id": 160,
    "name": "Panama",
    "dial_code": 507,
    "country_code": "pa",
    "name_ru": "\u041f\u0430\u043d\u0430\u043c\u0430",
    "name_en": "Panama",
    "name_es": "Panam\u00e1",
    "name_de": "Panama",
    "name_pl": "Panama"
  }, {
    "id": 161,
    "name": "Papua New Guinea",
    "dial_code": 675,
    "country_code": "pg",
    "name_ru": "\u041f\u0430\u043f\u0443\u0430 - \u041d\u043e\u0432\u0430\u044f \u0413\u0432\u0438\u043d\u0435\u044f",
    "name_en": "Papua New Guinea",
    "name_es": "Pap\u00faa Nueva Guinea",
    "name_de": "Papua-Neuguinea",
    "name_pl": "Papua Nowa Gwinea"
  }, {
    "id": 162,
    "name": "Paraguay",
    "dial_code": 595,
    "country_code": "py",
    "name_ru": "\u041f\u0430\u0440\u0430\u0433\u0432\u0430\u0439",
    "name_en": "Paraguay",
    "name_es": "Paraguay",
    "name_de": "Paraguay",
    "name_pl": "Paragwaj"
  }, {
    "id": 163,
    "name": "Peru",
    "dial_code": 51,
    "country_code": "pe",
    "name_ru": "\u041f\u0435\u0440\u0443",
    "name_en": "Peru",
    "name_es": "Per\u00fa",
    "name_de": "Peru",
    "name_pl": "Peru"
  }, {
    "id": 164,
    "name": "Philippines",
    "dial_code": 63,
    "country_code": "ph",
    "name_ru": "\u0424\u0438\u043b\u0438\u043f\u043f\u0438\u043d\u044b",
    "name_en": "Philippines",
    "name_es": "Filipinas",
    "name_de": "Philippinen",
    "name_pl": "Filipiny"
  }, {
    "id": 165,
    "name": "Pitcairn",
    "dial_code": null,
    "country_code": "pn",
    "name_ru": "\u041f\u0438\u0442\u043a\u044d\u0440\u043d",
    "name_en": "Pitcairn",
    "name_es": "Pitcairn",
    "name_de": "Pitcairn",
    "name_pl": "Pitcairn"
  }, {
    "id": 166,
    "name": "Poland",
    "dial_code": 48,
    "country_code": "pl",
    "name_ru": "\u041f\u043e\u043b\u044c\u0448\u0430",
    "name_en": "Poland",
    "name_es": "Polonia",
    "name_de": "Polen",
    "name_pl": "Polska"
  }, {
    "id": 167,
    "name": "Portugal",
    "dial_code": 351,
    "country_code": "pt",
    "name_ru": "\u041f\u043e\u0440\u0442\u0443\u0433\u0430\u043b\u0438\u044f",
    "name_en": "Portugal",
    "name_es": "Portugal",
    "name_de": "Portugal",
    "name_pl": "Portugalia"
  }, {
    "id": 168,
    "name": "Puerto Rico",
    "dial_code": 1,
    "country_code": "pr",
    "name_ru": "\u041f\u0443\u044d\u0440\u0442\u043e-\u0420\u0438\u043a\u043e",
    "name_en": "Puerto Rico",
    "name_es": "Puerto Rico",
    "name_de": "Puerto Rico",
    "name_pl": "Puerto Rico"
  }, {
    "id": 169,
    "name": "Qatar",
    "dial_code": 974,
    "country_code": "qa",
    "name_ru": "\u041a\u0430\u0442\u0430\u0440",
    "name_en": "Qatar",
    "name_es": "Qatar",
    "name_de": "Katar",
    "name_pl": "Katar"
  }, {
    "id": 170,
    "name": "R\u00e9union",
    "dial_code": 262,
    "country_code": "re",
    "name_ru": "\u0420\u0435\u044e\u043d\u044c\u043e\u043d",
    "name_en": "Reunion",
    "name_es": "Reuni\u00f3n",
    "name_de": "Reunion",
    "name_pl": "Reunion"
  }, {
    "id": 171,
    "name": "Romania",
    "dial_code": 40,
    "country_code": "ro",
    "name_ru": "\u0420\u0443\u043c\u044b\u043d\u0438\u044f",
    "name_en": "Romania",
    "name_es": "Ruman\u00eda",
    "name_de": "Rum\u00e4nien",
    "name_pl": "Rumunia"
  }, {
    "id": 172,
    "name": "Russia",
    "dial_code": 7,
    "country_code": "ru",
    "name_ru": "\u0420\u043e\u0441\u0441\u0438\u044f",
    "name_en": "Russia",
    "name_es": "Rusia",
    "name_de": "Russland",
    "name_pl": "Rosja"
  }, {
    "id": 173,
    "name": "Rwanda",
    "dial_code": 250,
    "country_code": "rw",
    "name_ru": "\u0420\u0443\u0430\u043d\u0434\u0430",
    "name_en": "Rwanda",
    "name_es": "Ruanda",
    "name_de": "Ruanda",
    "name_pl": "Rwanda"
  }, {
    "id": 174,
    "name": "Saint Barth\u00e9lemy",
    "dial_code": 590,
    "country_code": "bl",
    "name_ru": "\u0421\u0435\u043d-\u0411\u0430\u0440\u0442\u0435\u043b\u0435\u043c\u0438",
    "name_en": "Saint Barth\u00e9lemy",
    "name_es": "San Bartolom\u00e9",
    "name_de": "St. Barth\u00e9lemy",
    "name_pl": "Wsp\u00f3lnota Saint Barth\u00e9lemy"
  }, {
    "id": 175,
    "name": "Saint Lucia",
    "dial_code": 1758,
    "country_code": "lc",
    "name_ru": "\u0421\u0435\u043d\u0442-\u041b\u044e\u0441\u0438\u044f",
    "name_en": "Saint Lucia",
    "name_es": "Santa Luc\u00eda",
    "name_de": "St. Lucia",
    "name_pl": "Saint Lucia"
  }, {
    "id": 176,
    "name": "Samoa",
    "dial_code": 685,
    "country_code": "ws",
    "name_ru": "\u0421\u0430\u043c\u043e\u0430",
    "name_en": "Samoa",
    "name_es": "Samoa",
    "name_de": "Samoa",
    "name_pl": "Samoa"
  }, {
    "id": 177,
    "name": "San Marino",
    "dial_code": 378,
    "country_code": "sm",
    "name_ru": "\u0421\u0430\u043d-\u041c\u0430\u0440\u0438\u043d\u043e",
    "name_en": "San Marino",
    "name_es": "San Marino",
    "name_de": "San Marino",
    "name_pl": "San Marino"
  }, {
    "id": 178,
    "name": "Saudi Arabia",
    "dial_code": 966,
    "country_code": "sa",
    "name_ru": "\u0421\u0430\u0443\u0434\u043e\u0432\u0441\u043a\u0430\u044f \u0410\u0440\u0430\u0432\u0438\u044f",
    "name_en": "Saudi Arabia",
    "name_es": "Arabia Saud\u00ed",
    "name_de": "Saudi-Arabien",
    "name_pl": "Arabia Saudyjska"
  }, {
    "id": 179,
    "name": "Senegal",
    "dial_code": 221,
    "country_code": "sn",
    "name_ru": "\u0421\u0435\u043d\u0435\u0433\u0430\u043b",
    "name_en": "Senegal",
    "name_es": "Senegal",
    "name_de": "Senegal",
    "name_pl": "Senegal"
  }, {
    "id": 180,
    "name": "Serbia",
    "dial_code": 381,
    "country_code": "rs",
    "name_ru": "\u0421\u0435\u0440\u0431\u0438\u044f",
    "name_en": "Serbia",
    "name_es": "Serbia",
    "name_de": "Serbien",
    "name_pl": "Serbia"
  }, {
    "id": 181,
    "name": "Seychelles",
    "dial_code": 248,
    "country_code": "sc",
    "name_ru": "\u0421\u0435\u0439\u0448\u0435\u043b\u044c\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "Seychelles",
    "name_es": "Seychelles",
    "name_de": "Seychellen",
    "name_pl": "Seszele"
  }, {
    "id": 182,
    "name": "Sierra Leone",
    "dial_code": 232,
    "country_code": "sl",
    "name_ru": "\u0421\u044c\u0435\u0440\u0440\u0430-\u041b\u0435\u043e\u043d\u0435",
    "name_en": "Sierra Leone",
    "name_es": "Sierra Leona",
    "name_de": "Sierra Leone",
    "name_pl": "Sierra Leone"
  }, {
    "id": 183,
    "name": "Singapore",
    "dial_code": 65,
    "country_code": "sg",
    "name_ru": "\u0421\u0438\u043d\u0433\u0430\u043f\u0443\u0440",
    "name_en": "Singapore",
    "name_es": "Singapur",
    "name_de": "Singapur",
    "name_pl": "Singapur"
  }, {
    "id": 184,
    "name": "Slovakia",
    "dial_code": 421,
    "country_code": "sk",
    "name_ru": "\u0421\u043b\u043e\u0432\u0430\u043a\u0438\u044f",
    "name_en": "Slovakia",
    "name_es": "Eslovaquia",
    "name_de": "Slowakei",
    "name_pl": "S\u0142owacja"
  }, {
    "id": 185,
    "name": "Slovenia",
    "dial_code": 386,
    "country_code": "si",
    "name_ru": "\u0421\u043b\u043e\u0432\u0435\u043d\u0438\u044f",
    "name_en": "Slovenia",
    "name_es": "Eslovenia",
    "name_de": "Slowenien",
    "name_pl": "S\u0142owenia"
  }, {
    "id": 186,
    "name": "Solomon Islands",
    "dial_code": 677,
    "country_code": "sb",
    "name_ru": "\u0421\u043e\u043b\u043e\u043c\u043e\u043d\u043e\u0432\u044b \u043e\u0441\u0442\u0440\u043e\u0432\u0430",
    "name_en": "Solomon Islands",
    "name_es": "Islas Salom\u00f3n",
    "name_de": "Salomon-Inseln",
    "name_pl": "Wyspy Salomona"
  }, {
    "id": 187,
    "name": "Somalia",
    "dial_code": 252,
    "country_code": "so",
    "name_ru": "\u0421\u043e\u043c\u0430\u043b\u0438",
    "name_en": "Somalia",
    "name_es": "Somalia",
    "name_de": "Somalia",
    "name_pl": "Somalia"
  }, {
    "id": 188,
    "name": "South Africa",
    "dial_code": 27,
    "country_code": "za",
    "name_ru": "\u042e\u0436\u043d\u0430\u044f \u0410\u0444\u0440\u0438\u043a\u0430",
    "name_en": "South Africa",
    "name_es": "Sud\u00e1frica",
    "name_de": "S\u00fcd-Afrika",
    "name_pl": "Republika Po\u0142udniowej Afryki"
  }, {
    "id": 189,
    "name": "South Sudan",
    "dial_code": 211,
    "country_code": "ss",
    "name_ru": "\u042e\u0436\u043d\u044b\u0439 \u0421\u0443\u0434\u0430\u043d",
    "name_en": "South Sudan",
    "name_es": "Sud\u00e1n del Sur",
    "name_de": "S\u00fcdsudan",
    "name_pl": "Sudan Po\u0142udniowy"
  }, {
    "id": 190,
    "name": "Spain",
    "dial_code": 34,
    "country_code": "es",
    "name_ru": "\u0418\u0441\u043f\u0430\u043d\u0438\u044f",
    "name_en": "Spain",
    "name_es": "Espa\u00f1a",
    "name_de": "Spanien",
    "name_pl": "Hiszpania"
  }, {
    "id": 191,
    "name": "Sri Lanka",
    "dial_code": 94,
    "country_code": "lk",
    "name_ru": "\u0428\u0440\u0438-\u041b\u0430\u043d\u043a\u0430",
    "name_en": "Sri Lanka",
    "name_es": "Sri Lanka",
    "name_de": "Sri Lanka",
    "name_pl": "Sri Lanka"
  }, {
    "id": 192,
    "name": "Suriname",
    "dial_code": 597,
    "country_code": "sr",
    "name_ru": "\u0421\u0443\u0440\u0438\u043d\u0430\u043c",
    "name_en": "Suriname",
    "name_es": "Surinam",
    "name_de": "Surinam",
    "name_pl": "Surinam"
  }, {
    "id": 193,
    "name": "Swaziland",
    "dial_code": 268,
    "country_code": "sz",
    "name_ru": "\u0421\u0432\u0430\u0437\u0438\u043b\u0435\u043d\u0434",
    "name_en": "Swaziland",
    "name_es": "Suazilandia",
    "name_de": "Swasiland",
    "name_pl": "Suazi"
  }, {
    "id": 194,
    "name": "Sweden",
    "dial_code": 46,
    "country_code": "se",
    "name_ru": "\u0428\u0432\u0435\u0446\u0438\u044f",
    "name_en": "Sweden",
    "name_es": "Suecia",
    "name_de": "Schweden",
    "name_pl": "Szwecja"
  }, {
    "id": 195,
    "name": "Switzerland",
    "dial_code": 41,
    "country_code": "ch",
    "name_ru": "\u0428\u0432\u0435\u0439\u0446\u0430\u0440\u0438\u044f",
    "name_en": "Switzerland",
    "name_es": "Suiza",
    "name_de": "Schweiz",
    "name_pl": "Szwajcaria"
  }, {
    "id": 196,
    "name": "Taiwan",
    "dial_code": 886,
    "country_code": "tw",
    "name_ru": "\u0422\u0430\u0439\u0432\u0430\u043d\u044c",
    "name_en": "Taiwan",
    "name_es": "Taiw\u00e1n",
    "name_de": "Taiwan",
    "name_pl": "Tajwan"
  }, {
    "id": 197,
    "name": "Tajikistan",
    "dial_code": 992,
    "country_code": "tj",
    "name_ru": "\u0422\u0430\u0434\u0436\u0438\u043a\u0438\u0441\u0442\u0430\u043d",
    "name_en": "Tajikistan",
    "name_es": "Tayikist\u00e1n",
    "name_de": "Tadschikistan",
    "name_pl": "Tad\u017cykistan"
  }, {
    "id": 198,
    "name": "Tanzania",
    "dial_code": 255,
    "country_code": "tz",
    "name_ru": "\u0422\u0430\u043d\u0437\u0430\u043d\u0438\u044f",
    "name_en": "Tanzania",
    "name_es": "Tanzania",
    "name_de": "Tansania",
    "name_pl": "Tanzania"
  }, {
    "id": 199,
    "name": "Thailand",
    "dial_code": 66,
    "country_code": "th",
    "name_ru": "\u0422\u0430\u0438\u043b\u0430\u043d\u0434",
    "name_en": "Thailand",
    "name_es": "Tailandia",
    "name_de": "Thailand",
    "name_pl": "Tajlandia"
  }, {
    "id": 200,
    "name": "Timor-Leste",
    "dial_code": 670,
    "country_code": "tl",
    "name_ru": "\u0422\u0438\u043c\u043e\u0440-\u041b\u0435\u0448\u0442\u0438",
    "name_en": "Timor-Leste",
    "name_es": "Timor-Leste",
    "name_de": "Timor-Leste",
    "name_pl": "Timor Wschodni"
  }, {
    "id": 201,
    "name": "Togo",
    "dial_code": 228,
    "country_code": "tg",
    "name_ru": "\u0422\u043e\u0433\u043e",
    "name_en": "Togo",
    "name_es": "Togo",
    "name_de": "Togo",
    "name_pl": "Togo"
  }, {
    "id": 202,
    "name": "Tokelau",
    "dial_code": 690,
    "country_code": "tk",
    "name_ru": "\u0422\u043e\u043a\u0435\u043b\u0430\u0443",
    "name_en": "Tokelau",
    "name_es": "Tokelau",
    "name_de": "Tokelau",
    "name_pl": "Tokelau"
  }, {
    "id": 203,
    "name": "Tonga",
    "dial_code": 676,
    "country_code": "to",
    "name_ru": "\u0422\u043e\u043d\u0433\u0430",
    "name_en": "Tonga",
    "name_es": "Tonga",
    "name_de": "Tonga",
    "name_pl": "Tonga"
  }, {
    "id": 204,
    "name": "Trinidad and Tobago",
    "dial_code": 1868,
    "country_code": "tt",
    "name_ru": "\u0422\u0440\u0438\u043d\u0438\u0434\u0430\u0434 \u0438 \u0422\u043e\u0431\u0430\u0433\u043e",
    "name_en": "Trinidad and Tobago",
    "name_es": "Trinidad y Tobago",
    "name_de": "Trinidad und Tobago",
    "name_pl": "Trynidad i Tobago"
  }, {
    "id": 205,
    "name": "Tunisia",
    "dial_code": 216,
    "country_code": "tn",
    "name_ru": "\u0422\u0443\u043d\u0438\u0441",
    "name_en": "Tunisia",
    "name_es": "T\u00fanez",
    "name_de": "Tunesien",
    "name_pl": "Tunezja"
  }, {
    "id": 206,
    "name": "Turkey",
    "dial_code": 90,
    "country_code": "tr",
    "name_ru": "\u0422\u0443\u0440\u0446\u0438\u044f",
    "name_en": "Turkey",
    "name_es": "Turqu\u00eda",
    "name_de": "T\u00fcrkei",
    "name_pl": "Turcja"
  }, {
    "id": 207,
    "name": "Turkmenistan",
    "dial_code": 993,
    "country_code": "tm",
    "name_ru": "\u0422\u0443\u0440\u043a\u043c\u0435\u043d\u0438\u0441\u0442\u0430\u043d",
    "name_en": "Turkmenistan",
    "name_es": "Turkmenist\u00e1n",
    "name_de": "Turkmenistan",
    "name_pl": "Turkmenistan"
  }, {
    "id": 208,
    "name": "Tuvalu",
    "dial_code": 688,
    "country_code": "tv",
    "name_ru": "\u0422\u0443\u0432\u0430\u043b\u0443",
    "name_en": "Tuvalu",
    "name_es": "Tuvalu",
    "name_de": "Tuvalu",
    "name_pl": "Tuvalu"
  }, {
    "id": 209,
    "name": "Uganda",
    "dial_code": 256,
    "country_code": "ug",
    "name_ru": "\u0423\u0433\u0430\u043d\u0434\u0430",
    "name_en": "Uganda",
    "name_es": "Uganda",
    "name_de": "Uganda",
    "name_pl": "Uganda"
  }, {
    "id": 210,
    "name": "Ukraine",
    "dial_code": 380,
    "country_code": "ua",
    "name_ru": "\u0423\u043a\u0440\u0430\u0438\u043d\u0430",
    "name_en": "Ukraine",
    "name_es": "Ucrania",
    "name_de": "Ukraine",
    "name_pl": "Ukraina"
  }, {
    "id": 211,
    "name": "United Arab Emirates",
    "dial_code": 971,
    "country_code": "ae",
    "name_ru": "\u041e\u0431\u044a\u0435\u0434\u0438\u043d\u0435\u043d\u043d\u044b\u0435 \u0410\u0440\u0430\u0431\u0441\u043a\u0438\u0435 \u042d\u043c\u0438\u0440\u0430\u0442\u044b",
    "name_en": "United Arab Emirates",
    "name_es": "San Vicente y las Granadinas",
    "name_de": "Vereinigte Arabische Emirate",
    "name_pl": "Zjednoczone Emiraty Arabskie"
  }, {
    "id": 212,
    "name": "United Kingdom",
    "dial_code": 44,
    "country_code": "gb",
    "name_ru": "\u0421\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u043d\u043e\u0435 \u041a\u043e\u0440\u043e\u043b\u0435\u0432\u0441\u0442\u0432\u043e",
    "name_en": "United Kingdom",
    "name_es": "Santa Luc\u00eda",
    "name_de": "Vereinigtes K\u00f6nigreich",
    "name_pl": "Zjednoczone Kr\u00f3lestwo"
  }, {
    "id": 213,
    "name": "Uruguay",
    "dial_code": 598,
    "country_code": "uy",
    "name_ru": "\u0423\u0440\u0443\u0433\u0432\u0430\u0439",
    "name_en": "Uruguay",
    "name_es": "Uruguay",
    "name_de": "Uruguay",
    "name_pl": "Urugwaj"
  }, {
    "id": 214,
    "name": "Uzbekistan",
    "dial_code": 998,
    "country_code": "uz",
    "name_ru": "\u0423\u0437\u0431\u0435\u043a\u0438\u0441\u0442\u0430\u043d",
    "name_en": "Uzbekistan",
    "name_es": "Uzbekist\u00e1n",
    "name_de": "Usbekistan",
    "name_pl": "Uzbekistan"
  }, {
    "id": 215,
    "name": "Vanuatu",
    "dial_code": 678,
    "country_code": "vu",
    "name_ru": "\u0412\u0430\u043d\u0443\u0430\u0442\u0443",
    "name_en": "Vanuatu",
    "name_es": "Vanuatu",
    "name_de": "Vanuatu",
    "name_pl": "Vanuatu"
  }, {
    "id": 216,
    "name": "Venezuela",
    "dial_code": 58,
    "country_code": "ve",
    "name_ru": "\u0412\u0435\u043d\u0435\u0441\u0443\u044d\u043b\u0430",
    "name_en": "Venezuela",
    "name_es": "Venezuela",
    "name_de": "Venezuela",
    "name_pl": "Wenezuela"
  }, {
    "id": 217,
    "name": "Viet Nam",
    "dial_code": 84,
    "country_code": "vn",
    "name_ru": "\u0412\u044c\u0435\u0442\u043d\u0430\u043c",
    "name_en": "Viet Nam",
    "name_es": "Vietnam",
    "name_de": "Vietnam",
    "name_pl": "Wietnam"
  }, {
    "id": 218,
    "name": "Western Sahara",
    "dial_code": 212,
    "country_code": "eh",
    "name_ru": "\u0417\u0430\u043f\u0430\u0434\u043d\u0430\u044f \u0421\u0430\u0445\u0430\u0440\u0430",
    "name_en": "Western Sahara",
    "name_es": "Sahara Occidental",
    "name_de": "Westsahara",
    "name_pl": "Sahara Zachodnia"
  }, {
    "id": 219,
    "name": "Yemen",
    "dial_code": 967,
    "country_code": "ye",
    "name_ru": "\u0419\u0435\u043c\u0435\u043d",
    "name_en": "Yemen",
    "name_es": "Yemen",
    "name_de": "Jemen",
    "name_pl": "Jemen"
  }, {
    "id": 220,
    "name": "Zambia",
    "dial_code": 260,
    "country_code": "zm",
    "name_ru": "\u0417\u0430\u043c\u0431\u0438\u044f",
    "name_en": "Zambia",
    "name_es": "Zambia",
    "name_de": "Sambia",
    "name_pl": "Zambia"
  }, {
    "id": 221,
    "name": "Zimbabwe",
    "dial_code": 263,
    "country_code": "zw",
    "name_ru": "\u0417\u0438\u043c\u0431\u0430\u0431\u0432\u0435",
    "name_en": "Zimbabwe",
    "name_es": "Zimbabue",
    "name_de": "Simbabwe",
    "name_pl": "Zimbabwe"
  }
];
