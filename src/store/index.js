import {createStore} from 'vuex';

export default createStore({
  state: {
    fname: {
      value: '',
      pattern: /^[\p{L}\s-]{2,40}$/u,
      error: '',
    },
    email: {
      value: '',
      pattern: /^[-.+_a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i,
      error: '',
    },
    country: {
      value: '',
      error: '',
    },
    password: {
      value: '',
      pattern: /^(?=.*[0-9])(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%^&*\\/-]{8,31}$/,
      error: '',
    },
    phone: {
      value: '',
      pattern: /\d{5,13}/,
      valid: false,
      error: '',
    },
    statusSendForm: 'start',

    // init value
    defaultCountry: 'ua',
  },
  getters: {
    fnameVal: state => state.fname.value,
    fnameValid: state => state.fname.value !== '' && state.fname.error === '',
    fnameErr: state => state.fname.error,
    fnamePattern: state => state.fname.pattern,

    emailVal: state => state.email.value,
    emailValid: state => state.email.value !== '' && state.email.error === '',
    emailErr: state => state.email.error,
    emailPattern: state => state.email.pattern,

    countryVal: state => state.country.value,
    countryValid: state => state.country.value !== '' && state.country.error === '',
    countryErr: state => state.country.error,

    passwordVal: state => state.password.value,
    passwordValid: state => state.password.value !== '' && state.password.error === '',
    passwordErr: state => state.password.error,
    passwordPattern: state => state.password.pattern,
    passwordComplexity: (state, getters) => {
      const pass = getters.passwordVal;
      const passPattern = getters.passwordPattern;
      if (pass.length === 0) {
        return '';
      }
      if (pass.length < 8) {
        return 'low';
      } else if (pass.match(passPattern)) {
        return 'high';
      } else if (pass.length >= 8) {
        return 'medium';
      }
    },

    phoneVal: state => state.phone.value,
    phoneValid: state => state.phone.valid,

    statusSendForm: state => state.statusSendForm,

    defaultCountry: state => state.defaultCountry,
  },

  mutations: {
    setValue(state, {field, value}) {
      state[field].value = value;
    },
    setError(state, {field, errorMessage}) {
      state[field].error = errorMessage;
    },
    setStatusSendForm(state, status) {
      state.statusSendForm = status;
    },
    setValidPhone(state, valid) {
      state.phone.valid = valid;
    },
    setStatusForm(state, status) {
      state.statusForm = status;
    },
    sendForm(state, data) {
      console.log('sendForm', data);
    },
  },
  actions: {
    fnameInput({commit}, value) {
      if (value.match(/[^\p{L}\s-]/u)) {
        commit('setError', {
          field: 'fname',
          errorMessage: 'Only latin letters (A-Z) and hyphens (-) are allowed.',
        });
      } else {
        commit('setError', {
          field: 'fname',
          errorMessage: '',
        });
      }

      value = (value.match(/[^\p{L}\s-]/u))
        ? value.replaceAll(/[^\p{L}\s-]/ug, '').slice(0, 40)
        : value.slice(0, 40);
      commit('setValue', {field: 'fname', value});
    },
    fnameBlur({state, commit}, value) {
      value = value.trim();
      if (value.length < 2) {
        commit('setError', {
          field: 'fname',
          errorMessage: 'Length must be at least 2 characters.',
        });
      } else if (!state.fname.pattern.test(value)) {
        commit('setError', {
          field: 'fname',
          errorMessage: 'Only latin letters (A-Z) and hyphens (-) are allowed.',
        });
      }
    },

    countrySelect({ commit }, value) {
      commit('setError', {
        field: 'country',
        errorMessage: '',
      });
      commit('setValue', {
        field: 'country',
        value,
      })
    },

    emailInput({ commit }, value) {
      commit('setError', {
        field: 'email',
        errorMessage: '',
      });
      commit('setValue', {field: 'email', value});
    },

    emailBlur({ state, commit }, value) {
      value = value.trim();
      if (value.indexOf('+') >= 0) {
        commit('setError', {
          field: 'email',
          errorMessage: 'Invalid format.',
        });
      } else if (!state.email.pattern.test(value)) {
        commit('setError', {
          field: 'email',
          errorMessage: 'Invalid format.',
        });
      }
    },

    passwordInput({ commit }, value) {
      commit('setError', {
        field: 'password',
        errorMessage: '',
      });
      commit('setValue', {field: 'password', value});
    },

    passwordBlur({ commit, getters }) {
      const { passwordVal, passwordPattern } = getters;
      if (!passwordVal.match(passwordPattern)) {
        commit('setError', {
          field: 'password',
          errorMessage: 'Password must contain at least one latin character and one number',
        });
      }
    },

    phoneInput({ commit }, { value, valid }) {
      // console.log(value);
      // console.log(valid);
      commit('setError', {
        field: 'phone',
        errorMessage: '',
      });
      commit('setValue', {field: 'phone', value});
      commit('setValidPhone', valid);
    },

    sendForm({ getters, commit }) {
      if (!getters.fnameValid) {
        commit('setError', {
          field: 'fname',
          errorMessage: 'Field is required.',
        });
      }

      if (!getters.countryValid) {
        commit('setError', {
          field: 'country',
          errorMessage: 'Field is required.',
        });
      }

      if (!getters.emailValid) {
        commit('setError', {
          field: 'email',
          errorMessage: 'Field is required.',
        });
      }

      if (!getters.passwordValid) {
        commit('setError', {
          field: 'password',
          errorMessage: 'Field is required.',
        });
      }

      if (!(getters.fnameValid && getters.emailValid)) {
        return;
      }

      commit('setStatusSendForm', 'pending');
      setTimeout(function () {
        commit('setStatusSendForm', 'done');
      }, 2000);

      // const response = await fetch('https://lp.monfex.com/api/action/Xw2ejdQucmaP/register', {
      //     method: 'post',
      //     headers: { 'Content-Type': 'application/json;charset=utf-8' },
      //     body: JSON.stringify({
      //         first_name: state.fname.value,
      //     }),
      // });
      // commit('setStatusForm', 'done');
      // const result = await response.json();
      // console.log(result);
    },
  },
});